function searchBook() {
    var search = document.getElementById('search').value
    document.getElementById('result').innerHTML = ""
    console.log(search)

    $.ajax({
        url: "https://www.googleapis.com/books/v1/volumes?q=" + search,
        dataType: "json",

        success: function(data) {
            result.innerHTML += "<tbody>"
            for (i = 0; i < data.items.length; i++){
                try {
                    result.innerHTML += "<tr id='row" + i + "'>" + "<td id='title" + i + "'>" + 
                    data.items[i].volumeInfo.title + "</td>" + "<td id='author" + i + "'>" + 
                    data.items[i].volumeInfo.authors[0] + "</td>" + 
                    "<td><lk id='likes" + i + "'>0</lk><button id='button" + i + "' onclick=clickButton('likes" + i + "')>Like</button></td>" + "</tr>"
                } catch (TypeError) {
                    result.innerHTML += "<tr id='row" + i + "'>" + "<td id='title" + i + "'>" + 
                    data.items[i].volumeInfo.title + "</td>" + "<td><em>Author Not Found</em></td>" +
                    "<td><lk id='likes" + i + "'>0</lk><button id='button" + i + "' onclick=clickButton('likes" + i + "'); sort())>Like</button></td>" + "</tr>"
                }
            }
            result.innerHTML += "</tbody>"

            
        },

        type: 'GET'
    });


}

function clickButton(a) {
    likeBook(a)
    sort()
    updateModal()
}

function likeBook(a) {
    var currLike = parseInt(document.getElementById(a).textContent)
    document.getElementById(a).innerHTML = currLike + 1
}

function sort() {
    var table, rows, switching, i, x, y, shouldSwitch;
    table = document.getElementById('bookTable')
    switching = true;
    while (switching) {
        switching = false;
        rows = table.rows;
        for (i = 1; i < (rows.length - 1); i++) {
            shouldSwitch = false;
            x = rows[i].getElementsByTagName('TD')[2];
            y = rows[i + 1].getElementsByTagName('TD')[2];
            if (parseInt(x.textContent) < parseInt(y.textContent)) {
                shouldSwitch = true;
                break;
            }
        }
        if (shouldSwitch) {
            rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
            switching = true;
        }
    }
}

function updateModal() {
    var modal = document.getElementById("modalTitle")
    modal.innerHTML = ''
    var books = document.getElementById("result")
    var booksRow = books.rows
    for(i = 0; i < 5; i++) {
        modal.innerHTML += "<tr>" + "<td id='modalBook" + i + "'>" + booksRow[i].cells[0].textContent  + "</td>" + "</tr>"
    }
}

document.getElementById('searchButton').addEventListener('click', searchBook)