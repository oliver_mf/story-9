from django.test import Client, TestCase, LiveServerTestCase
from django.urls import resolve
from django.apps import apps
from .views import *
from selenium import webdriver
from homepage.apps import HomepageConfig
import unittest
import time

# Create your tests here.
class unitTest(TestCase):
    def test_index_url_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code,200)

    def test_index_template(self):
        response = Client().get('')
        self.assertTemplateUsed(response, 'index.html')

    def test_index_function(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

#====================================================================================

class functionalTest(LiveServerTestCase):
    def setUp(self):
        super().setUp()
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.driver = webdriver.Chrome(chrome_options=chrome_options, executable_path='chromedriver')


    def tearDown(self):
        self.driver.quit()

    def test_page_title(self):
        self.driver.get(self.live_server_url)
        self.assertIn("Story 9", self.driver.title)

    def test_search(self):
        self.driver.get(self.live_server_url)
        self.driver.find_element_by_id("search").send_keys("mickey")
        self.driver.find_element_by_id("searchButton").click()
        time.sleep(3)
        result = self.driver.find_element_by_id("row0")
        self.assertIn("Mickey", result.text)

    def test_like_button_(self):
        self.driver.get(self.live_server_url)
        self.driver.find_element_by_id("search").send_keys("mickey")
        self.driver.find_element_by_id("searchButton").click()
        time.sleep(3)
        self.driver.find_element_by_id("button0").click()
        count = self.driver.find_element_by_id("likes0")
        self.assertEqual("1", count.text)
        count2 = self.driver.find_element_by_id("likes1")
        self.assertEqual("0", count2.text)

    def test_modal_update(self):
        self.driver.get(self.live_server_url)
        self.driver.find_element_by_id("search").send_keys("Hyouka")
        self.driver.find_element_by_id("searchButton").click()
        time.sleep(3)
        self.driver.find_element_by_id("button1").click()
        self.driver.find_element_by_id("button1").click()
        self.driver.find_element_by_id("button0").click()
        self.driver.find_element_by_id("modalButton").click()
        time.sleep(3)
        first = self.driver.find_element_by_xpath("/html/body/div[1]/div/div/div/div[2]/table/tbody/tr[1]/td")
        second = self.driver.find_element_by_xpath("/html/body/div[1]/div/div/div/div[2]/table/tbody/tr[2]/td")
        self.assertIn("Kyoto Animation", first.text)
        self.assertIn("Hyouka", second.text)